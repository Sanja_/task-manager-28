/*
package ru.karamyshev.taskmanager.endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.marker.ClientTestCategory;

import java.util.ArrayList;
import java.util.List;

@Category(ClientTestCategory.class)
public class TaskEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    @Before
    public void init() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();

        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");

        final List<Task> taskList = new ArrayList<>();
        final Task task = new Task();
        task.setName("taskDemo");
        task.setDescription("taskDemo");
        task.setUserId(session.getUserId());
        task.setId(111111111L);
        taskList.add(task);
        taskEndpoint.loadTask(session, taskList);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void createNameTaskTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        Assert.assertNotNull(session);
        final int amountTask = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createName(session, "task-1");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), amountTask + 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void createDescriptionTaskTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        Assert.assertNotNull(session);
        final int amountTask = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createDescription(session, "task-1", "task-1");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), amountTask + 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void addTaskTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        Assert.assertNotNull(session);
        final Task task = new Task();
        task.setName("task-1");
        task.setId(111111L);
        final int amountTask = taskEndpoint.getTaskList(session).size();
        taskEndpoint.addTask(session, task);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), amountTask + 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void getTaskListTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int amountTask = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertNotNull(session);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), amountTask + 3);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findAllTaskTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int amountTask = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.findAllTask(session).size(), amountTask + 3);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findOneTaskByIndexTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        taskEndpoint.createName(session, "task-1");
        final Task task = taskEndpoint.findTaskOneByIndex(session, 2);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), "task-1");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findOneTaskByNameTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        taskEndpoint.createName(session, "task-1");
        final List<Task> tasks = taskEndpoint.findTaskOneByName(session, "task-1");
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(tasks.get(0).getName(), "task-1");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findOneTaskByIdTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        taskEndpoint.createName(session, "task-1");
        final List<Task> tasks = taskEndpoint.findTaskOneByName(session, "task-1");
        final Task task = taskEndpoint.findTaskOneById(session, String.valueOf(tasks.get(0).getId()));
        Assert.assertNotNull(String.valueOf(tasks.get(0).getId()), task.getId());
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void clearTaskTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int amountTask = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.findAllTask(session).size(), amountTask + 3);
        taskEndpoint.clearTask(session);
        Assert.assertEquals(taskEndpoint.findAllTask(session).size(), 0);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void updateTaskByIdTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        taskEndpoint.createName(session, "task-1");
        final Task task = taskEndpoint.findTaskOneByIndex(session, 1);
        Assert.assertNotNull(task);
        final Task taskUpdate = taskEndpoint.updateTaskById(session, String.valueOf(task.getId()),
                "user-11", "user-11");
        Assert.assertEquals(taskUpdate.getName(), "user-11");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void updateTaskByIndexTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        taskEndpoint.createName(session, "task-1");
        final Task task = taskEndpoint.findTaskOneByIndex(session, 1);
        Assert.assertNotNull(task);
        final Task taskUpdate = taskEndpoint.updateTaskByIndex(session, 1,
                "user-11", "user-11");
        Assert.assertNotNull(taskUpdate);
        Assert.assertEquals(taskUpdate.getName(), "user-11");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeOneTaskByIndexTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int taskSize = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), taskSize + 3);
        taskEndpoint.removeTaskOneByIndex(session, 1);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), taskSize + 2);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeOneTaskByNameTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int taskSize = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), taskSize + 3);
        taskEndpoint.removeTaskOneByName(session, "task-1");
        Assert.assertEquals(taskEndpoint.findTaskOneByName(session, "task-1").size(), 0);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), taskSize + 2);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeOneTaskByIdTest() throws java.lang.Exception {
        final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int taskSize = taskEndpoint.getTaskList(session).size();
        taskEndpoint.createName(session, "task-1");
        taskEndpoint.createName(session, "task-2");
        taskEndpoint.createName(session, "task-3");
        final Task task = taskEndpoint.findTaskOneByIndex(session, 1);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), taskSize + 3);
        taskEndpoint.removeTaskOneById(session, String.valueOf(task.getId()));
        Assert.assertEquals(taskEndpoint.getTaskList(session).size(), taskSize + 2);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

}
*/
