package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.util.TerminalUtil;


import java.lang.Exception;
import java.util.List;

public class TaskShowByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskvwnm";
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskEndpoint().findTaskOneByName(session, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
            showTask(task);

        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("\n");
        System.out.println("ID:" + task.getId());
        System.out.println("NAME:" + task.getName());
        System.out.println("DESCRIPTION:" + task.getDescription());
    }

    @NotNull
    public Role[] roles(){
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
