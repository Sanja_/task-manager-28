package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;


public class ServerInfoCommand extends AbstractCommand {
    @NotNull
    @Override
    public String arg() {
        return "-srvinf";
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @Override
    public @NotNull String description() {
        return "Show server information.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[INFORMATION SERVER]");

        @Nullable final SessionDTO currentSession = serviceLocator.getSessionService().getSession();
        System.out.println("HOST :");
/*        System.out.println(serviceLocator.getSessionEndpoint().getServerHost(currentSession));
        System.out.println("PORT :");
        System.out.println(serviceLocator.getSessionEndpoint().getServerPort(currentSession));*/
        System.out.println("[OK]");
    }

}
