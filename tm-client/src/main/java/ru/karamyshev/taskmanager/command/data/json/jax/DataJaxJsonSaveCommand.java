package ru.karamyshev.taskmanager.command.data.json.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;


public class DataJaxJsonSaveCommand extends AbstractDataCommand {

   @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-json-save";
    }

    @Override
    public @NotNull String description() {
        return "Save data from json(jax-b) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON(JAX-B) SAVE]");
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().saveDataJaxBJson(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
