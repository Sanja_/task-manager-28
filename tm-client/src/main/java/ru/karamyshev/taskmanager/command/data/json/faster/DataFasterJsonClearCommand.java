package ru.karamyshev.taskmanager.command.data.json.faster;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

import java.io.Serializable;

public class DataFasterJsonClearCommand extends AbstractDataCommand implements Serializable {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-fs-json-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove json(faster) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE JSON(FASTER) FILE]");
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().clearDataFasterJson(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
