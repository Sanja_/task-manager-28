package ru.karamyshev.taskmanager.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.dto.ProjectDTO;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.repository.ProjectRepository;
import ru.karamyshev.taskmanager.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new Exception();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(serviceLocator.getUserService().findById(userId));

        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(entityManager);

        @Nullable final List<Project> projectList =
                (projectRepository.findAllByUserId(userId));
        entityManager.close();
        return ProjectDTO.toDTO(projectList);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projectList = projectRepository.findAll();
        entityManager.close();
        return ProjectDTO.toDTO(projectList);
    }

    @Override
    public void clearAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final Project project = projectRepository.findOneByName(userId, name);
        entityManager.close();

        return project;
    }

    @Override
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return;
        project.setName(name);
        project.setDescription(description);

        @NotNull final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        try {
            entityManager.getTransaction().begin();
            entityManager.merge(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final IProjectRepository projectRepository =
                new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final Project project = projectRepository.findOneById(userId, id);
        entityManager.close();

        return project;
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final IProjectRepository projectRepository =
                new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() {
        @Nullable final EntityManager entityManager =
                serviceLocator.getEntityManagerService().getEntityManager();

        @Nullable final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
