package ru.karamyshev.taskmanager.exception.user;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
