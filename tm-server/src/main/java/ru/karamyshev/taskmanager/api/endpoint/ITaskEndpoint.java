package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String taskName,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String projectName
    ) throws Exception;

    @WebMethod
    void createDescription(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "nameTask", partName = "nameTask") @Nullable String name,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String projectName,
            @WebParam(name = "descriptionTask", partName = "descriptionTask") @Nullable String description
    ) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> findAllTaskByUserId(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception;

    @WebMethod
    void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception;

    @WebMethod
    void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception;

    @WebMethod
    void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description
    ) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> getTaskList(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @WebMethod
    void loadTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "tasks", partName = "tasks") @Nullable List<Task> tasks
    ) throws Exception;

}
